#!/usr/bin/env zsh

# ZSH Defaults
unsetopt beep
unsetopt PROMPT_SP
stty stop undef		# Disable ctrl-s to freeze terminal.
setopt autocd		# Automatically cd into typed directory.
setopt interactive_comments

plugins=(
    asdf
    colored-man-pages
    zsh-autosuggestions
    zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh
source $ZDOTDIR/shell_expand
for file in $XDG_CONFIG_HOME/shell/{aliasrc,localrc,p10krc}; do
    [ -r "$file" ] && [ -f "$file" ] && source "$file";
done;

eval "$(zoxide init --cmd cd zsh)"
eval "$(starship init zsh)"
