LNX_OS="$(awk -F= '$1=="ID" { print $2 ;}' /etc/os-release)"
#################################[ os_icon: os identifier ]##################################
# OS identifier color. (Default Color)
typeset -g POWERLEVEL9K_OS_ICON_FOREGROUND=012
typeset -g POWERLEVEL9K_OS_ICON_BACKGROUND=000

if [[ $LNX_OS == 'debian' ]]; then
    typeset -g POWERLEVEL9K_OS_ICON_FOREGROUND=124
    typeset -g POWERLEVEL9K_OS_ICON_BACKGROUND=232
elif [[ $LNX_OS == 'endeavouros' ]]; then
    # Custom icon.
    # if Endeavouros the Custom Icon
    typeset -g POWERLEVEL9K_OS_ICON_CONTENT_EXPANSION=''
elif [[ $LNX_OS == 'gentoo' ]]; then
    typeset -g POWERLEVEL9K_OS_ICON_FOREGROUND=183
    typeset -g POWERLEVEL9K_OS_ICON_BACKGROUND=000
fi
