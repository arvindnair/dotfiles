# Shiroryuu's Dotfiles

These dotfiles are highly personalized and might not be a perfect fit for everyone. I encourage you to use them as inspiration and a reference for building your own setup. Feel free to borrow and adapt what you find useful, and if you like something, please give credit!

This repository contains core dotfiles compatible with multiple operating systems. OS-specific dotfiles are located in separate repositories.

## Deployment

I use ansible to deploy these dotfiles.

## Essential Tools

These tools form the foundation of my development and productivity workflow.

| Category             | Programs                           | My Config                                           |
| :------------------- | :--------------------------------- | :-------------------------------------------------- |
| Terminal Emulator    | [Alacritty][Alacritty-Link]        | In this repo                                        |
| Editor               | [Neovim][Neovim-Link]              | [My config](https://codeberg.org/arvindnair/nvdots) |
| Shell                | ZSH with [OMZ][OMZ-LINK] framework | In this repo                                        |
| Shell prompt         | [Starship][Starship-Link]          | In this repo                                        |
| Terminal Multiplexer | [Zellij][Zellij-Link]              | In this repo                                        |

### Linux Specific

| Category              | Programs                                                  |
| :-------------------- | :-------------------------------------------------------- |
| Application Launcher  | [Rofi][Rofi-Link] (X11) and [Tofi][Tofi-Link] (Wayland)   |
| Desktop Notifications | [Dunst][Dunst-Link] (X11) and [Mako][Mako-Link] (Wayland) |
| Compositor            | [picom][Picom-Link] (X11 only)                            |
| Resource Monitor      | [btop][Btop-Link]                                         |

## Window Managers

I switch between these depending on my setup:

| Display Server | Window Manager            | Configs                                                     |
| :------------- | :------------------------ | :---------------------------------------------------------- |
| Wayland        | [Hyprland][Hyprland-Link] | [Hyprland Config](https://codeberg.org/arvindnair/hyprdots) |
| Xorg/X11       | [Dwm][Dwm-Link]           | [DWM Config](https://codeberg.org/arvindnair/dwm)           |

## Screenshot

### DWM

![alt Dwm Setup](screenshots/dwm-setup.png)

### Hyprland (Currently using)

![alt Hyprland Setup](screenshots/hyprland-setup.png)

## License

Everything in this repository is licensed under the T&Cs of [MIT License][MIT-LISC]

## Author

By: Arvind Hariharan Nair

[Alacritty-Link]: https://github.com/alacritty/alacritty
[Btop-Link]: https://github.com/aristocratos/btop
[Dunst-Link]: https://github.com/dunst-project/dunst
[Dwm-Link]: https://dwm.suckless.org/
[Mako-Link]: https://github.com/emersion/mako
[MIT-LISC]: https://codeberg.org/arvindnair/dotfiles/src/branch/main/master/LICENSE
[Neovim-Link]: https://github.com/neovim/neovim
[OMZ-LINK]: https://github.com/ohmyzsh/ohmyzsh
[Picom-Link]: https://github.com/yshui/picom
[Rofi-Link]: https://github.com/davatorium/rofi
[Starship-Link]: https://github.com/starship/starship
[Tofi-Link]: https://github.com/philj56/tofi
[Zellij-Link]: https://github.com/zellij-org/zellij
